#!/usr/bin/python

from select import select
from pyspark.sql import*
import pymongo, gridfs,sys
import pika, json, os
from pyspark.ml.recommendation import ALS,ALSModel
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.sql.types import StructType, StructField, StringType,IntegerType,BooleanType,DateType,DoubleType
from pyspark.ml.feature import StringIndexer,IndexToString
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder,TrainValidationSplit
#os.chmod('tenants/models',stat.S_IRWXU)
from pyspark.sql.functions import lit
import pickle

import io


#import requests

from dotenv import load_dotenv
load_dotenv() 


class CRUD:
    def __init__(self,param) -> None:
        self.client=None
        self.database=None
        self.URI = os.environ.get("BASEURI") + param + os.environ.get("SUFFIXURI")
        self.connection()
        self.database = self.client.get_database(name=param)
        
    def save(self,name):
        try:
            fs = gridfs.GridFS(self.database)
            with io.FileIO(name, 'r') as fileObject:
                    docId = fs.put(fileObject, filename=name)  
            print(docId)
        except ValueError:
            sys.exit(1)
    def get(self):
        try:
           
            r=list(self.database.ratings.aggregate([{
                "$lookup":{
                    "from":"items",
                    "localField":"itemId",
                    "foreignField":"_id",
                    "as":"rating-item"
                },
            }]))
            return r
            
        except ValueError:
            sys.exit(1)

    def update(self):
        try:
            pass
            
        except ValueError:
            sys.exit(1)

    def connection(self):
         self.client = pymongo.MongoClient(self.URI)


class Consumer:
    def __init__(self,connection) -> None:
        self.connection=connection
        self.channel=None
       
       # self.spark.sparkContext.setCheckpointDir('tenants/models')
        #self.spark.sparkContext.conf('job.local.dir', 'tenant/models')
    def callback(self,ch, method, properties, body):
        data= json.loads(body)

   
        self.spark = SparkSession.builder.config('spark.mongodb.write.connection.uri', data['uri']+'.recommendations').getOrCreate() #parkContext.getOrCreate(SparkConf().setMaster('local[*]'))
        #spark.mongodb.write.connection.uri
      
        ml =ML.start(spark=self.spark,uri=data['uri'],user=data['user'])
        ml.getRating(data['data'])
    
  
    

       # self.spark.stop()
       # print(model.avgMetrics)
     
        
    @classmethod
    def connect(self):
        params = pika.URLParameters(os.environ.get('AMQP_BOOTSTRAP_SERVER'))
        connection = pika.BlockingConnection(params)
        return self(connection=connection)

    def start(self):
        self.channel = self.connection.channel()
    
    def stop(self):
        self.channel.close()

    def consuming(self):
        self.channel.basic_consume(queue='training', on_message_callback=self.callback, auto_ack=True)
        self.channel.start_consuming()


class ML():
    def __init__(self,spark,uri=None,user=None):
        self.spark=spark
        self.ratings=None
        self.Model=None
        self.data_set=None
        self.schema=None
        self.user= user
        #self.crud = CRUD(param=uri)
        self.set_Schema()
        self.items=None
        self.dataframe=None
        self.stages=None
        self.df = None
        self.itemdf=None
        self.transforming=None
        self.model_params={
            "als":{
               "model":self.Model,
               "params":{
                   "maxIter":[5,15,20],
                   "alpha":[0.1,0.5,0.8,1,5,15],
                   "seed":[0,5,6],
                   "rank":[5,10],
               }
            }
        }
       

    @classmethod
    def start(self,spark,uri,user):
        return self(spark=spark,uri=uri,user=user)

    def model(self):
        self.Model= ALS(rank=3, maxIter=5, alpha = 1.0, seed=0)
        self.Model.setMaxIter(10)
        self.Model.setUserCol("user")
        self.Model.setItemCol("product")
        self.Model.setRatingCol("rating")
        self.Model.setColdStartStrategy('drop')
        return self.Model
  
  
    def get_training_data(self):
        self.df.createOrReplaceTempView('ratings')
        self.ratings= self.spark.sql("SELECT user, product,rating FROM ratings")
        self.ratings.show()
        return self

    def load_data(self):
        r=self.crud.get()
        self.items=r
        return self
    def dataFrame(self,item):
       
        self.getRating(item)
    
        return self

    def getItems(self):
        tab=list()
        for key,val in enumerate(self.items):
            val['_id']=str(val['_id'])
            val['rating-item'][0].pop('itemUrl')
            tab.extend(val['rating-item'])
        self.itemdf=self.spark.createDataFrame(tab,self.set_ItemSchema())
        return self.itemdf

    def getRating(self,item):
        self.df= self.spark.createDataFrame(item,self.schema)
        self.df.show()
        return self
        
        
    
    def set_Schema(self):
        self.schema = StructType([StructField("itemId", StringType()),
                     StructField("rating", IntegerType()),
                     StructField("customerId", StringType()),
                     StructField("_id", StringType()),
                     StructField("purchased", BooleanType()),
                     StructField("updated_at", StringType()),
                     StructField("created_at",StringType()),
                     StructField("timestamp",StringType()),
                     StructField("detailViews", BooleanType()),
                     StructField("__v", IntegerType())
                     ])
       
    def set_UserSchema(self):
        schema = StructType([
                     StructField("customerId", StringType()),
                     ])
    
        return schema    


    def set_ItemSchema(self):
        return StructType([
                     StructField("__v", IntegerType()),
                     StructField("itemId", StringType()),
                     StructField("updated_at", DateType()),
                     StructField("created_at",DateType()),
                     StructField("_id", StringType())
                     ])

            
    def pipeline(self):
       self.pipeline= Pipeline(stages=self.stages)
       return self

    def fit(self):
        self.transforming= self.pipeline.fit(self.df)
        return self
   
    def transform(self):
        self.df=  self.transforming.transform(self.df)   
        return self
    
    def featurizer(self):
        item_indexer,user_indexer = StringIndexer(inputCol="itemId",outputCol="product"),StringIndexer(inputCol="customerId",outputCol="user")
        
        self.stages=[item_indexer,user_indexer]

        return self
    def userDataFrame(self):
        #self.df.createOrReplaceTempView('userDataFrame')

        user= self.df.filter(self.df.customerId==self.user)
        user=user.select(user.user).distinct().limit(1)    #self.spark.sql("SELECT user, customerId FROM userDataFrame WHERE userDataFrame{}customerId={} LIMIT 1".format('.',self.user))
          
        return user

       
    

    def evaluate(self,predictions):
        evaluator = RegressionEvaluator(metricName="rmse", labelCol="rating",
                                predictionCol="prediction")
                                
        rmse = evaluator.evaluate(predictions)
        df=self.spark.createDataFrame([Row(metric="rmse",value=rmse)])
        return df
    
    def estimator(self,names=['map']):
    
        return RegressionEvaluator(predictionCol='prediction',labelCol="rating",metricName='rmse')

    def cross_validation(self):
            model =self.model()
            paramGrid = ParamGridBuilder() \
            .addGrid(model.alpha, [10, 20, 40]) \
            .addGrid(model.maxIter, [5,10]) \
                  .addGrid(model.blockSize, [5,10,20]) \
            .build()
            crossval = CrossValidator(estimator=model,
                                estimatorParamMaps=paramGrid,
                                evaluator=self.estimator(),
                                numFolds=2,
                                parallelism=2,collectSubModels=True)

            cvModel =crossval.fit(self.ratings) 
            self.ratings.show()
            return cvModel

    def split_train(self):
            model =self.model()
            paramGrid = ParamGridBuilder() \
            .addGrid(model.alpha, [10, 20, 40]) \
            .addGrid(model.maxIter, [5,8]) \
            .addGrid(model.rank, [5,10]) \
            .build()
            tsp= TrainValidationSplit( 
                 estimatorParamMaps=paramGrid,
                evaluator=self.estimator(),estimator=model)
            tsp.fit(self.ratings)
            return self 
    def getItemId(self):
        pass
    
    def test_data(self):
        test = self.spark.createDataFrame([Row(user=2,product=10,rating=5),Row(user=10,product=0,rating=5),Row(user=5,product=1,rating=75),Row(user=9,product=10,rating=85)])
        return test
    def recommendTopItemToUser():
        pass
    
if __name__=="__main__":

    try:
        consumer= Consumer.connect()
        consumer.start()
        consumer.consuming()
    except ValueError:
         consumer.stop()
  

