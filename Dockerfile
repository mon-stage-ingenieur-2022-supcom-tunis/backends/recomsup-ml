FROM bitnami/spark:3.2.1
ENV PYTHONUNBUFFERED=1
COPY requirements.txt ./
USER root
RUN pip install -r requirements.txt
RUN pip install "pymongo[srv]"